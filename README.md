 Certificado Digital será utilizado por nós em dois momentos:
 
1 – Quando acessamos os WebServices do Sefaz
2 – Quando assinamos alguns (não todos) arquivos XML para transmissão
 
Não é o intuito deste post discursar sobre chave pública e privada, criptografia, segurança digital e implicações na utilização de Certificados. Como falamos, este grupo prioriza a praticidade. Ou seja, pouca teoria e mais prática (teoria tem aos montes por ai, basta dar uma “googlada”). Portanto, veremos aqui, como acessar os servidores do Sefaz mediante uma comunicação segura via Java, utilizando certificados do tipo A1 (em formato de arquivo).
 
KeyStore e TrustStore
Para que você possa acessar os servidores do Sefaz você deve garantir a eles quem você é e, também, ter a garantia de que eles são quem dizem ser.
O KeyStore é o repositório (banco de dados) do Java onde você armazena SEUS certificados digitais, ou seja, suas assinaturas, as credencias que confirmam sua identidade.
O TrustStore é um repositório de certificados de terceiros, digamos assim. Nele são registrados os certificados de confiança do cliente, neste caso os certificados de cada um dos Sefaz que você pretende acessar.
Em resumo, o KeyStore é utilizado para autenticar um cliente (garante que você é quem diz ser) e o TrusStore para autenticar um servidor (garante que ele é quem diz ser).
Desta forma, quando você acessar o WebService de envio de uma NF-e/NFC-e, por exemplo, sua identidade será confirmada pelos registro contido no KeyStore e a identidade do Sefaz do seu estado  pelos registros do TrustStore.
 
Gerando KeyStore e TrustStore
Você deve estar pensando: Ok, muito bem, compreendi o que são esses camaradas ai, mas onde eles estão? Como os configuro? Como os acesso? De onde venho? Para onde vou?
Na verdade, apesar do nome feio deles, é relativamente fácil trabalhar com KeyStore e TrustStore, uma vez que não são nada mais, nada menos, que arquivos que serão gerados e armazenados no computador que acessará os serviços da receita federal.
 
Primeiro o KeyStore
Como é necessário somente um certificado para acessarmos os WebServices da Receita Federal nosso KeyStore será nosso próprio certificado, este acrescido da cadeia de certificados da AC, para que seja aceito corretamente.
Para gerar este “certificado completo” basta utilizar o assistente de certificados do Windows para importar seu certificado e, a seguir, exportá-lo juntamente com sua cadeia de certificados. Vamos ver como fazer isso agora.