package br.com.dos;

public class Model {

	private String NomeCliente;
	private String endereco;
	private String DEENDEREÇO;
	private String DECOMPLEMENTO;
	private String NMBAIRRO;
	private String NUCEP;
	private String CDNUMERO;
	private String CDMUNICIPIO;
	private String nmmunicipio;
	private String CpfCnpj;
	private String CodEmpreendimento;
	private String NumeroContrato;
	private String Unidade;
	private String DataContrato;
	private String QtdParcelasUnicas;
	private String QtdParcelasMensais;
	private String ValorPagoAtoVenda;
	private String IntermediaçãoTotal;
	private String ValorParcelaMensalContratada;
	private String Dt1Vencto;
	private String TotalParcelaMensal;
	private String Distratado;
	private String ParcelaLoteFoiDistratado;
	private String ValorParcelaAtual;
	private String NumeroParcelasAberto;
	private String NumeroParcelasPagas;
	
	public Model(){
		this.NomeCliente = "";
		this.endereco = "";
		this.DEENDEREÇO = "";
		this.DECOMPLEMENTO = "";
		this.NMBAIRRO = "";
		this.NUCEP = "";
		this.CDNUMERO = "";
		this.CDMUNICIPIO = "";
		this.nmmunicipio = "";
		this.CpfCnpj = "";
		this.CodEmpreendimento = "";
		this.NumeroContrato = "";
		this.Unidade = "";
		this.DataContrato = "";
		this.QtdParcelasUnicas = "";
		this.QtdParcelasMensais = "";
		this.ValorPagoAtoVenda = "";
		this.IntermediaçãoTotal = "";
		this.ValorParcelaMensalContratada = "";
		this.Dt1Vencto = "";
		this.TotalParcelaMensal = "";
		this.Distratado = "";
		this.ParcelaLoteFoiDistratado = "";
		this.ValorParcelaAtual = "";
		this.NumeroParcelasAberto = "";
		this.NumeroParcelasPagas = "";
	}
	public String getNomeCliente() {
		return NomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		NomeCliente = nomeCliente == null ? "" : nomeCliente;
		;
	}

	public String getEndereco() {
		return "";
	}

	public String getDEENDEREÇO() {
		return DEENDEREÇO;
	}

	public void setDEENDEREÇO(String dEENDEREÇO) {
		DEENDEREÇO = dEENDEREÇO == null ? "" : dEENDEREÇO;
	}

	public String getDECOMPLEMENTO() {
		return DECOMPLEMENTO;
	}

	public void setDECOMPLEMENTO(String dECOMPLEMENTO) {
		DECOMPLEMENTO = dECOMPLEMENTO == null ? "" : dECOMPLEMENTO;
	}

	public String getNMBAIRRO() {
		return NMBAIRRO;
	}

	public void setNMBAIRRO(String nMBAIRRO) {
		NMBAIRRO = nMBAIRRO == null ? "" : nMBAIRRO;
	}

	public String getNUCEP() {
		return NUCEP;
	}

	public void setNUCEP(String nUCEP) {
		NUCEP = nUCEP == null ? "" : nUCEP;
	}

	public String getCDNUMERO() {
		return CDNUMERO;
	}

	public void setCDNUMERO(String cDNUMERO) {
		CDNUMERO = cDNUMERO == null ? "" : cDNUMERO;
	}

	public String getCDMUNICIPIO() {
		return CDMUNICIPIO;
	}

	public void setCDMUNICIPIO(String cDMUNICIPIO) {
		CDMUNICIPIO = cDMUNICIPIO == null ? "" : cDMUNICIPIO;
		;
	}

	public String getNmmunicipio() {
		return nmmunicipio;
	}

	public void setNmmunicipio(String nmmunicipio) {
		this.nmmunicipio = nmmunicipio == null ? "" : nmmunicipio;
		;
	}

	public String getCpfCnpj() {
		return CpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		CpfCnpj = cpfCnpj == null ? "" : cpfCnpj;
	}

	public String getCodEmpreendimento() {
		return CodEmpreendimento;
	}

	public void setCodEmpreendimento(String codEmpreendimento) {
		CodEmpreendimento = codEmpreendimento == null ? "" : codEmpreendimento;
	}

	public String getNumeroContrato() {
		return NumeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		NumeroContrato = numeroContrato == null ? "" : numeroContrato;
	}

	public String getUnidade() {
		return Unidade;
	}

	public void setUnidade(String unidade) {
		Unidade = unidade == null ? "" : unidade;
	}

	public String getDataContrato() {
		return DataContrato;
	}

	public void setDataContrato(String dataContrato) {
		DataContrato = dataContrato == null ? "" : dataContrato;
	}

	public String getQtdParcelasUnicas() {
		return QtdParcelasUnicas;
	}

	public void setQtdParcelasUnicas(String qtdParcelasUnicas) {
		QtdParcelasUnicas = qtdParcelasUnicas == null ? "" : qtdParcelasUnicas;
	}

	public String getQtdParcelasMensais() {
		return QtdParcelasMensais;
	}

	public void setQtdParcelasMensais(String qtdParcelasMensais) {
		QtdParcelasMensais = qtdParcelasMensais == null ? "" : qtdParcelasMensais;
	}

	public String getValorPagoAtoVenda() {
		return ValorPagoAtoVenda;
	}

	public void setValorPagoAtoVenda(String valorPagoAtoVenda) {
		ValorPagoAtoVenda = valorPagoAtoVenda == null ? "" : valorPagoAtoVenda;
	}

	public String getIntermediaçãoTotal() {
		return IntermediaçãoTotal;
	}

	public void setIntermediaçãoTotal(String intermediaçãoTotal) {
		IntermediaçãoTotal = intermediaçãoTotal == null ? "" : intermediaçãoTotal;
	}

	public String getValorParcelaMensalContratada() {
		return ValorParcelaMensalContratada;
	}

	public void setValorParcelaMensalContratada(String valorParcelaMensalContratada) {
		ValorParcelaMensalContratada = valorParcelaMensalContratada == null ? "" : valorParcelaMensalContratada;
		;
	}

	public String getDt1Vencto() {
		return Dt1Vencto;
	}

	public void setDt1Vencto(String dt1Vencto) {
		Dt1Vencto = dt1Vencto == null ? "" : dt1Vencto;
		;
	}

	public String getTotalParcelaMensal() {
		return TotalParcelaMensal;
	}

	public void setTotalParcelaMensal(String totalParcelaMensal) {
		TotalParcelaMensal = totalParcelaMensal == null ? "" : totalParcelaMensal;
	}

	public String getDistratado() {
		return Distratado;
	}

	public void setDistratado(String distratado) {
		if (distratado != null) {
			if (distratado == "3") {
				Distratado = "sim";
			} else if (distratado == "2") {
				Distratado = "nao";
			}
		} else {
			Distratado = "";
		}
	}

	public String getParcelaLoteFoiDistratado() {
		return ParcelaLoteFoiDistratado;
	}

	public void setParcelaLoteFoiDistratado(String parcelaLoteFoiDistratado) {
		ParcelaLoteFoiDistratado = parcelaLoteFoiDistratado == null ? "" : parcelaLoteFoiDistratado;
	}

	public String getValorParcelaAtual() {
		return ValorParcelaAtual;
	}

	public void setValorParcelaAtual(String valorParcelaAtual) {
		ValorParcelaAtual = valorParcelaAtual == null ? "" : valorParcelaAtual;
	}

	public String getNumeroParcelasAberto() {
		return NumeroParcelasAberto;
	}

	public void setNumeroParcelasAberto(String numeroParcelasAberto) {
		NumeroParcelasAberto = numeroParcelasAberto == null ? "" : numeroParcelasAberto;
	}

	public String getNumeroParcelasPagas() {
		return NumeroParcelasPagas;
	}

	public void setNumeroParcelasPagas(String numeroParcelasPagas) {
		NumeroParcelasPagas = numeroParcelasPagas == null ? "" : numeroParcelasPagas;
	}

}
