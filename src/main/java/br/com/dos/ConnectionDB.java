package br.com.dos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Diogo on 15/07/2017.
 */
public class ConnectionDB {
    private static Connection connection = null;
    private final String DB_DRIVER = "org.firebirdsql.jdbc.FBDriver";

	private final String DB_NAME = "jdbc:firebirdsql:localhost/3050:";

	private static final String DB_USERNAME = "SYSDBA";

	private static final String DB_PASSWORD = "masterkey";
	
	private static final String DB_FILE = "masterkey";

    public ConnectionDB() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        //para rodar no local
        connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/dados", "root",
                "mysql");
        
        /*Class.forName(DB_DRIVER);
        connection = DriverManager.getConnection(DB_FILE,
        		DB_USERNAME, DB_PASSWORD);*/
        
        
    }
    
    
    public static Connection getConnection() throws SQLException {

        if (connection == null) {
            try {
                new ConnectionDB();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ConnectionDB.class.getName()).log(
                        Level.SEVERE, null, ex);
            }
        }

        return connection;
    }
}