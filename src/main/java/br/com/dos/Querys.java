package br.com.dos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Querys {
	Connection con = null;
	public Querys() throws SQLException{
		this.con =  ConnectionDB.getConnection();
	}
	public ArrayList<Model> queryPrincipal() throws SQLException {
		String query = "SELECT ecadcliente.nmcliente, ecadclifis.nucpf, ecadclijur.nucnpj, evndunidadecontr.nuunidade, ecadendcliente.tpendereco, ecadendcliente.cdmunicipio, ecadmunicipio.nmmunicipio, ecadendcliente.deendereco, ecadendcliente.decomplemento, ecadendcliente.nmbairro, ecadendcliente.cdnumero, ecadendcliente.nucep, ecadempreend.cdempreendview, ecadmunicipio.nmmunicipio, evndcontrato.nucontratoview, evndcontrato.dtcontrato, evndcontrato.flsituacao, evndcontratocondPU.vltotalcondpu, evndcontratocondPU.qtparcelaspu, evndcontratocondPU.sumvltotalcondpu, evndcontratocondPM.qtparcelaspm, evndcontratocondPM.dt1venctopm, evndcontratocondPM.vltotalcondpm, evndcontratocondPM.vlpgatovendapm FROM ecadendcliente LEFT JOIN ecadcliente ON ecadendcliente.cdcliente = ecadcliente.cdcliente AND ecadendcliente.tpendereco = 'R' LEFT JOIN ecadclifis ON ecadclifis.cdcliente = ecadcliente.cdcliente LEFT JOIN ecadclijur ON ecadclijur.cdcliente = ecadcliente.cdcliente LEFT JOIN ecadempreend ON ecadempreend.cdobra = ecadempreend.cdempreend AND ecadempreend.nucnpj = ecadclijur.nucnpj LEFT JOIN ecadmunicipio ON ecadendcliente.cdmunicipio = ecadmunicipio.cdmunicipio LEFT JOIN evndcontrato ON evndcontrato.cdempreend = ecadempreend.cdempreend LEFT JOIN evndunidadecontr ON evndunidadecontr.cdempresa = evndcontrato.cdempresa AND evndunidadecontr.cdempreend = evndcontrato.cdempreend AND evndunidadecontr.nucontrato = evndcontrato.nucontrato LEFT JOIN (SELECT evndcontratocond.cdempresa, evndcontratocond.cdempreend, evndcontratocond.nucontrato, evndcontratocond.cdtipocondicao, evndcontratocond.qtparcelas as qtparcelaspu, evndcontratocond.vltotalcond as vltotalcondpu, sum(evndcontratocond.vltotalcond) as sumvltotalcondpu FROM evndcontratocond WHERE evndcontratocond.cdtipocondicao = 'PU' GROUP BY evndcontratocond.cdempresa, evndcontratocond.cdempreend, evndcontratocond.nucontrato) AS evndcontratocondPU ON evndcontratocondPU.cdempresa= evndcontrato.cdempresa AND evndcontratocondPU.cdempreend= evndcontrato.cdempreend AND evndcontratocondPU.nucontrato= evndcontrato.nucontrato LEFT JOIN (SELECT evndcontratocond.cdempresa, evndcontratocond.cdempreend, evndcontratocond.nucontrato, evndcontratocond.qtparcelas as qtparcelaspm, evndcontratocond.dt1vencto as dt1venctopm, evndcontratocond.vltotalcond as vltotalcondpm, evndcontratocond.vltotalcond/evndcontratocond.qtparcelas as vlpgatovendapm FROM evndcontratocond WHERE evndcontratocond.cdtipocondicao = 'PM' GROUP BY evndcontratocond.cdempresa, evndcontratocond.cdempreend, evndcontratocond.nucontrato) AS evndcontratocondPM ON evndcontratocondPM.cdempresa= evndcontrato.cdempresa AND evndcontratocondPM.cdempreend= evndcontrato.cdempreend AND evndcontratocondPM.nucontrato= evndcontrato.nucontrato WHERE ecadcliente.nmcliente IS NOT NULL GROUP BY ecadcliente.nmcliente";
		Model m = null;
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		ArrayList<Model> list = new ArrayList<>();
		while (rs.next()) {
			m = new Model();
								
			m.setNomeCliente(rs.getString("nmcliente"));
			m.setCpfCnpj(rs.getString("nucpf") !=null ? rs.getString("nucpf"):rs.getString("nucnpj"));
			m.setDEENDEREÇO(rs.getString("deendereco"));
			m.setDECOMPLEMENTO(rs.getString("decomplemento"));
			m.setNMBAIRRO(rs.getString("nmbairro"));
			m.setNUCEP(rs.getString("nucep"));
			m.setUnidade(rs.getString("nuunidade"));
			m.setCDNUMERO(rs.getString("cdnumero"));
			m.setCDMUNICIPIO(rs.getString("cdmunicipio"));
			m.setNmmunicipio(rs.getString("nmmunicipio"));
			m.setCodEmpreendimento(rs.getString("cdempreendview"));
			m.setNumeroContrato(rs.getString("nucontratoview"));
			m.setDataContrato(rs.getString("dtcontrato"));
			m.setDistratado(rs.getString("flsituacao"));
			m.setQtdParcelasUnicas(rs.getString("qtparcelaspu"));
			m.setValorPagoAtoVenda(rs.getString("vltotalcondpu"));
			m.setIntermediaçãoTotal(rs.getString("sumvltotalcondpu"));
			m.setQtdParcelasMensais(rs.getString("qtparcelaspm"));
			m.setDt1Vencto(rs.getString("dt1venctopm"));
			m.setValorParcelaMensalContratada(rs.getString("vlpgatovendapm"));
			m.setTotalParcelaMensal(rs.getString("vltotalcondpm"));
			m.setValorParcelaAtual("");
			m.setParcelaLoteFoiDistratado("");
			//m.setCodEmpreendimento(codEmpreendimento);
			//m.setNumeroContrato(numeroContrato);
			if(m.getDistratado() == "sim"){
				m.setParcelaLoteFoiDistratado("");
			}else{
				m.setValorParcelaAtual("");
				m.setNumeroParcelasAberto("");
				m.setNumeroParcelasPagas("");
			}
			
			list.add(m);
		}
		return list;
	}
}
