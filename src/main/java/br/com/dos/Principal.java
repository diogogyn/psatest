package br.com.dos;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Principal {
	/*
	 * Connection con = null; Principal() throws SQLException{ con =
	 * ConnectionDB.getConnection(); }
	 */
	/*
	Nome do Cliente - ECADCLIENTE (NMCLIENTE)
	Endereço - ECADENDCLIENTE (TPENDEREÇO=R, DEENDEREÇO, DECOMPLEMENTO, NMBAIRRO, NUCEP, CDNUMERO, CDMUNICIPIO=BUSCAR TABELA ECADMUNICIPIO)
	CPF / CNPJ - ECADCLIFIS (NUCPF) OU ECADCLIJUR (NUCNPJ)
	Cod. Empreendimento - ECADEMPREEND (CDEMPREENDVIEW)
	Número de Contrato - EVNDCONTRATO (NUCONTRATOVIEW**)
	Unidade - EVNDUNIDADECONTR (NUUNIDADE)
	Data do contrato - EVNDCONTRATO (DTCONTRATO)
	Qt. de parcelas ùnicas - EVNDCONTRATOCOND (CDTIPOCONDIÇÃO=PU)
	Qd. de parcelas mensais - EVNDCONTRATOCOND (CDTIPOCONDIÇÃO=PM)
	Valor pago no ato da venda - EVNDCONTRATOCOND (= valor da primeira parcela CDTIPOCONDIÇAO=PU)
	Intermediação Total - EVNDCONTRATOCOND (= soma todas parcelas CDTIPOCONDIÇAO=PU)
	Valor da Parcela mensal contratada - EVNDCONTRATOCOND (= valor total dividido pelo número parcelas do CDTIPOCONDIÇAO=PM)
 	Dt. 1º Vencto (PM) - EVNDCONTRATOCOND (DT1VENCTO=PM)
	Total Parcela Mensal (Vl Histórico) - EVNDCONTRATOCOND (VLTOTALCOND=PM)
	Distratado - EVNDCONTRATO (FLSITUACAO=3 será SIM, =2 será NÃO)
	Parcela em que lote foi distratado - se Distratado = SIM busca na tabela ECRCPARCELA campo FLSITUACAO=2 e busca tabela ECRCBAIXA campo CDTIPOBAIXA=7; busca NUPARCELA (relacionamento campo NUDOCUMENTO com NUTITULO)
	Valor da parcela Atual - se Distratado = NÃO, ECRCPARCELA (= parcela do CDTIPOCONDIÇAO=PM e DTVENCTO=mês atual)
	Número de parcelas em aberto - se Distratado = NÃO, ECRCPARCELA (= parcela do CDTIPOCONDIÇAO=PM e FLSITUACAO=0)
	Número de parcelas pagas - se Distratado = NÃO, ECRCPARCELA (= parcela do CDTIPOCONDIÇAO=PM e FLSITUACAO=2)

	 */
	
	public static void  DbToCSV() throws SQLException {
		//Connection con =  ConnectionDB.getConnection();
		//C:\Users\Diogo\Desktop
		//String filename = "Desktop:test.csv";
		String filename = "C:\\Users\\Diogo\\Desktop\\test.csv";
		try {
			FileWriter fw = new FileWriter(filename);
			Querys q = new Querys();
			//String query = "select * from certificado";
			List<Model> list = q.queryPrincipal(); 
			List<String> lista = null;
			for(Model l:list){
				System.out.println(l.getNomeCliente());
				lista = new ArrayList<>();
				System.out.println(l.getNomeCliente());
				System.out.println(l.getEndereco());
				System.out.println(l.getCpfCnpj());
				System.out.println(l.getCodEmpreendimento());
				System.out.println(l.getNumeroContrato());
				System.out.println(l.getUnidade());
				System.out.println(l.getDataContrato());
				System.out.println(l.getQtdParcelasUnicas());
				System.out.println(l.getQtdParcelasMensais());
				System.out.println(l.getValorPagoAtoVenda());
				System.out.println(l.getIntermediaçãoTotal());
				System.out.println(l.getValorParcelaMensalContratada());
				System.out.println(l.getDt1Vencto());
				System.out.println(l.getTotalParcelaMensal());
				System.out.println(l.getDistratado());
				System.out.println(l.getParcelaLoteFoiDistratado());
				System.out.println(l.getValorParcelaAtual());
				System.out.println(l.getNumeroParcelasAberto());
				System.out.println(l.getNumeroParcelasPagas());
				
				lista.add(l.getNomeCliente());
				lista.add(l.getEndereco());
				lista.add(l.getCpfCnpj());
				lista.add(l.getCodEmpreendimento());
				lista.add(l.getNumeroContrato());
				lista.add(l.getUnidade());
				lista.add(l.getDataContrato());
				lista.add(l.getQtdParcelasUnicas());
				lista.add(l.getQtdParcelasMensais());
				lista.add(l.getValorPagoAtoVenda());
				lista.add(l.getIntermediaçãoTotal());
				lista.add(l.getValorParcelaMensalContratada());
				lista.add(l.getDt1Vencto());
				lista.add(l.getTotalParcelaMensal());
				lista.add(l.getDistratado());
				lista.add(l.getParcelaLoteFoiDistratado());
				lista.add(l.getValorParcelaAtual());
				lista.add(l.getNumeroParcelasAberto());
				lista.add(l.getNumeroParcelasPagas());
				CSVUtils.writeLine(fw, lista);
				
				
			}
			fw.flush();
			fw.close();
			//con.close();
			System.out.println("CSV File is created successfully.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		fw.append(l.getNomeCliente());
		fw.append(',');
		fw.append(l.getEndereco());
		fw.append(',');
		fw.append(l.getCpfCnpj());
		fw.append(',');
		fw.append(l.getCodEmpreendimento());
		fw.append(',');
		fw.append(l.getNumeroContrato());
		fw.append(',');
		fw.append(l.getUnidade());
		fw.append(',');
		fw.append(l.getDataContrato());
		fw.append(',');
		fw.append(l.getQtdParcelasUnicas());
		fw.append(',');
		fw.append(l.getQtdParcelasMensais());
		fw.append(',');
		fw.append(l.getValorPagoAtoVenda());
		fw.append(',');
		fw.append(l.getIntermediaçãoTotal());
		fw.append(',');
		fw.append(l.getValorParcelaMensalContratada());
		fw.append(',');
		fw.append(l.getDt1Vencto());
		fw.append(',');
		fw.append(l.getTotalParcelaMensal());
		fw.append(',');
		fw.append(l.getDistratado());
		fw.append(',');
		fw.append(l.getParcelaLoteFoiDistratado());
		fw.append(',');
		fw.append(l.getValorParcelaAtual());
		fw.append(',');
		fw.append(l.getNumeroParcelasAberto());
		fw.append(',');
		fw.append(l.getNumeroParcelasPagas());
		fw.append('\n');
		*/
	}

	public static void main(String[] args) throws SQLException {
		DbToCSV();
	}
}